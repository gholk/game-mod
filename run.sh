#!/bin/sh

run_html() {
    cd public/haze-of-despair
    mark-dd -t template.html README.md > index.html
    mark-dd -t template.html -e dd.main.offline=true README.md > 使用說明.html
}

run_gitlab() {
    cd public
    mv haze-of-despair/使用說明.html .
    mv haze-of-despair HazeOfDespair
    zip -r HazeOfDespair.zip HazeOfDespair 使用說明.html
    mv HazeOfDespair haze-of-despair
    mv 使用說明.html HazeOfDespair.zip haze-of-despair
}

run_$1
