# 霧影絕境 Tabletop Club 模組
本模組是基於 <u>迷幻酵母工作室</u> 所製作的 *合作型奇幻風桌遊* [霧影絕境] ，
經工作室授權後由 <u>gholk</u> 製作的
可於 PC 遊戲 [Tabletop Club] 使用的 3D 素材模組，
安裝後可以於 Tabletop Club 中遊玩霧影絕境。

本模組下載網址： <https://gholk.gitlab.io/game-mod/haze-of-despair/HazeOfDespair.zip>

[霧影絕境]: https://www.facebook.com/HazeOfDespair/
[Tabletop Club]: https://drwhut.itch.io/tabletop-club

<style>
blockquote {
border-left: #DDD 1ex solid;
padding-left: 1em;
margin-left: 0.5ex;
}
code {
border: 1px solid;
padding: 0 0.5ex;
}
pre > code:only-child {
display: block;
padding: 1ex;
}
#disqus_thread:empty {
display: none;
}
</style>

Tabletop Club 是一款開放原始碼的桌遊模擬器類型遊戲，
設計與操作上類似於知名的同類遊戲 [Tabletop Simulater] ，
操作教學請參考 tts 與 ttc 遊戲內操作提示。
若您喜歡請考慮付費購買以支持其開發。
（本模組作者 gholk 與 Tabletop Club 開發者無利害關係）

[Tabletop Simulater]: https://www.tabletopsimulator.com/

霧影絕境是由迷幻酵母工作室所製作的合作型奇幻風桌遊，
本模組由迷幻酵母工作室以創用 CC BY-NC 4.0 (姓名標示-非商業性) 授權發布。

> CC BY-NC 4.0 法律條款<br>
> 姓名標示-非商業性 4.0 國際<br>
> https://creativecommons.org/licenses/by-nc/4.0/legalcode.zh-hant
> 
> 霧影絕境-合作型奇幻風桌遊 迷幻酵母工作室授權
> 
> 粉絲團網址 https://www.facebook.com/HazeOfDespair/

## 安裝
初次執行 Tabletop Club 後， [TTC](..abbr Tabletop Club) 會於您家目錄下的
`Documents` 或 `文件` 資料夾中建立 `TabletopClub` 資料夾，
請將此模組解壓縮，把 HazeOfDespair 資料夾置於 `TabletopClub/assets` 資料夾內，
再重新啟動遊戲，之後即可於工具列的 `物件` 或 `遊戲` 中載入霧影絕境。

範例：

```
Documents
`--TabletopClub
   |-- assets
   |   `-- HazeOfDespair
   |       |-- cards
   |       |   |-- config.cfg
   |       |   |-- (...skip)
   |       |   `-- item-10.jpg
   |       |-- games
   |       |   |-- Haze of Despair.png
   |       |   `-- Haze of Despair.tc
   |       |-- license-CC-BY-NC.txt
   |       |-- README.md
   |       |-- templates
   |       |   |-- config.cfg
   |       |   `-- manual.jpg
   |       `-- tokens
   |           `-- cube
   |               |-- config.cfg
   |               |-- (...skip)
   |               `-- role-7.jpg
   |-- saves
   `-- screenshots
```

詳細模組使用教學請見 [Tabletop Club 的英文教學][howto asset pack] 。

[howto asset pack]: https://tabletop-club.readthedocs.io/en/stable/custom_assets/tutorials/create_asset_pack.html

模組中使用八面骰代替六面骰表示場景卡的傷害，
多餘八面骰收納於桌面上的收納罐中。

規則書請於工具列的 `筆記 / 新頁面` 中載入，
或直接閱讀位於 `HazeOfDespair/template/manual.jpg` 的規則書。

## 聯絡資訊
  - [gitlab issue](https://gitlab.com/gholk/game-mod/-/issues)
  - [mail to gholk](mailto:xhrv257y+gholk.mc@anonaddy.me)
  - [霧影絕境-迷幻酵母工作室][霧影絕境]

## 模組版本
```
v2024.03.10.gholk
```

## 留言

<div id="disqus_thread" class="online"></div>

<button class="online" onclick="disqus_load(event)">載入 disqus 留言</button>
<a class="offline" href="https://disqus.com/home/discussion/game-mod/tabletop_club/">在 disqus 上留言</a>
<a href="https://g0v.social/@gholk/112082803741408934" type="text/html+ActivityPub" rel="alternative">在 ActivityPub/mastodon 上留言</a>

<script class="online">
let disqus_config = function () {
  this.page.url = 'https://gholk.gitlab.io/game-mod/haze-of-despair/'
  this.page.identifier = 'game-mod/haze-of-despair'
}
function disqus_load(event) {
  const s = document.createElement('script')
  s.src = 'https://game-mod.disqus.com/embed.js'
  s.setAttribute('date-timestamp', Date.now())
  document.body.appendChild(s)
  if (event.target.nodeName == 'BUTTON') event.target.remove()
}
</script>

<link rel="webmention" href="https://webmention.io/gholk.github.io/webmention">
<link rel="pingback" href="https://webmention.io/gholk.github.io/xmlrpc">

<script class="template">
if (dd.main.offline) $('.online').remove()
else $('.offline').remove()
$('html>head').append($('body link'))
$('p:empty').remove()
</script>
